class CreateDataJsons < ActiveRecord::Migration
  def change
    create_table :data_jsons do |t|
      t.string :woeid
      t.string :trend
      t.string :location
      t.string :iroiro

      t.timestamps
    end
  end
end
