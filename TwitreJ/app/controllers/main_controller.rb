require 'uri'

class MainController < ApplicationController
  def index

  	def prepare_access_token(customer_key, customer_secret, oauth_token, oauth_token_secret)
      consumer = OAuth::Consumer.new(customer_key, customer_secret,
	      { :site => "http://api.twitter.com",
	        :scheme => :header
	      })
	    # now create the access token object from passed values
	    token_hash = { :oauth_token => oauth_token,
	                   :oauth_token_secret => oauth_token_secret
	                 }
	    access_token = OAuth::AccessToken.from_hash(consumer, token_hash )
	    return access_token
	  end

    # Exchange our oauth_token and oauth_token secret for the AccessToken instance.
    access_token1 = prepare_access_token("2nGp3907wMTYnyiJujuH4dK0W", "LsuDiF6Bt7PA82hVb8Sv2IMyJJ1bzbneUW27a00HNlXtTClZS8", "2785443780-9fUJSYDkp1QGx9PiSYXXEPTAZnFqdVoXs5GRsZj", "3WWenHLMf3I8EaD0q11UygwPCm4dCT97CFfogp7XX16zu")
    access_token2 = prepare_access_token("gNxAS46F3vPe6yk4FgbNOoIb1", "OF782yPBOiOzYTr2BaVIiaSIQLuXBTsgAeQWjucp2S3TthPL2x", "2785434062-UR2t0t2tGf6Eek96rj6ZL2hTHnmwXk54wKwDvIk", "GLMEr0mu1R6A4nXQV2jk5p2PD3Ix7cgAgKqjM3Lgri3BQ")


    data = DataJson.first

    if data == nil then
      last_access_time = 0
    else
      last_access_time = data.updated_at
    end
    puts Time.new - last_access_time

    if data == nil || (Time.new - last_access_time) > 1800 then
      # # DataJson.new(key:"test_key", value:"test_value").save

      # # woeid と country_name を取得
      json_country = JSON.parse(access_token1.request(:get, "https://api.twitter.com/1.1/trends/available.json").body)
      woeids = []
      locations = []
      json_country.each do |json|
        # 日本のものだけ取ってくる
        if json["countryCode"] == "JP" then
          if json["woeid"] == 23424856 then
          elsif json["woeid"] == 1117502 then
          else
            woeids.push(json["woeid"])
            locations.push(json["name"])
          end
        end
      end
      gon.woeids = woeids
      gon.locations = locations

      # trends 取得
      trends = []
      for i in 0..19 do
        puts i
        if i < 10 then
          response_trends = access_token1.request(:get, "https://api.twitter.com/1.1/trends/place.json?exclude=hashtags&id=" + woeids[i].to_s)
          json_trends = JSON.parse(response_trends.body)
          trends.push(json_trends[0]["trends"][0]["name"])
        else
          response_trends = access_token2.request(:get, "https://api.twitter.com/1.1/trends/place.json?exclude=hashtags&id=" + woeids[i].to_s)
          json_trends = JSON.parse(response_trends.body)
          trends.push(json_trends[0]["trends"][0]["name"])
        end
      end

      gon.trend = trends
      # iroiro に trends を投げて RGB を取得
      iroiros = []
      puts trends
      trends.each do |trend|
        response_iroiro = Net::HTTP.post_form(URI.parse('http://synthsky.com/iroiro/q'), {'format'=>'json', 'req'=>trend})
        json_iroiro = JSON.parse(response_iroiro.body)
        iroiros.push(json_iroiro["scheme"]["elements"][1]["rgb"])
      end
      gon.iroiros = iroiros
      puts iroiros

      woeids.each_index do |index|
        puts woeids[index]
        if DataJson.find_by(woeid:woeids[index].to_s) then
          tmpData = DataJson.find_by(woeid:woeids[index].to_s)
          tmpData.woeid = woeids[index].to_s
          tmpData.location = locations[index].to_s
          tmpData.trend = trends[index].to_s
          tmpData.iroiro = iroiros[index].to_s
          tmpData.updated_at = Time.new
          puts tmpData.id
          tmpData.save
        else
          DataJson.new(woeid:woeids[index], location:locations[index], trend:trends[index], iroiro:iroiros[index]).save
        end
      end
      datas = DataJson.all

    else
      datas = DataJson.all
    end 

    # woeid, locations, trends, iroiros を JSON形式に変換して画面に返す
    retdata = {}
    retdatas = []
    datas.each do |data|
      retdata.store("woeid", data.woeid)
      retdata.store("location", data.location)
      retdata.store("trend", data.trend)
      retdata.store("iroiro", data.iroiro)
      retdatas.push(retdata)
      retdata = {}
    end
    gon.result = retdatas
    gon.json = JSON.generate(retdatas)
    gon.format_json = JSON.pretty_generate(retdatas)

  end
end
